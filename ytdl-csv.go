// Package main is the script to automate using youtube-dl from a CSV list.
//
// This script assumes that youtube-dl is installed an defined in the system's
// $PATH environment variable.
//
// If the CSV file has two-element lines, the 2nd element is used as the file name.
// If the CSV file has one-element lines, the 1st element is used to derive the filename.
// Derivation takes the base directory of the m3u8 file, adds a CRC-32 checksum and uses
// an mp4 extension.
package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"hash/crc32"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

var flagDir = flag.String("download-dir", "", "Directory where files are downloaded.  Default is current directory.")
var flagListFile = flag.String("list-file", "", "Path to CSV containing list of files to download.")
var flagNumJobs = flag.Int("j", 6, "Number of simultaneous download jobs to run.")
var flagReferer = flag.String("referer", "", "Referer URL if required.")
var flagUserAgent = flag.String("user-agent", "", "Custom user agent string to use.")

type Status int

const (
	Started Status = iota
	Finished
	Error
)

const DateFormat = "2006/01/02 15:04"

const UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"

type downloadJob struct {
	// URL is the URL from which the file is to be downloaded.
	URL string
	// Name is the name of the file as it should be written to disk.
	Name string
}

type statusMessage struct {
	// Job is the download job
	Job downloadJob
	// Status is the status of the download job
	Status Status
	// Error is the optional error if Status = Error
	Error error
	// Duration is the duration the job took
	time.Duration
}

func ValidateFlags() error {
	i, err := os.Stat(*flagDir)
	if err != nil {
		return fmt.Errorf("error opening --download-dir %q: %s", *flagDir, err)
	}
	if !i.IsDir() {
		return fmt.Errorf("error --download-dir %q is not a directory", *flagDir)
	}
	f, err := os.Open(*flagListFile)
	defer f.Close()
	if err != nil {
		return fmt.Errorf("error opening --list-file %q: %s", *flagListFile, err)
	}
	if *flagNumJobs <= 0 {
		return fmt.Errorf("-j cannot be zero or negative: %q", *flagNumJobs)
	}
	return nil
}

func MakeDlJobFromLine(line []string) (*downloadJob, error) {
	var fileName string
	job := new(downloadJob)
	url := line[0]
	switch len(line) {
	case 1:
		urlFile := filepath.Base(url)
		if strings.HasPrefix(urlFile, "master.m3u8") {
			fileName = fmt.Sprintf("%s-%d.mp4", filepath.Base(filepath.Dir(url)), crc32.ChecksumIEEE([]byte(filepath.Dir(url))))
		} else {
			fileName = urlFile + ".mp4"
		}
	case 2:
		fileName = line[1]
	default:
		return nil, fmt.Errorf("want 1 or 2 elements, got %d elements", len(line))
	}

	job.Name = filepath.Join(*flagDir, fileName)
	job.URL = url
	return job, nil
}

func ReadDownloadJobs() ([]downloadJob, error) {
	f, err := os.Open(*flagListFile)
	if err != nil {
		return nil, err
	}
	r := csv.NewReader(f)
	// Prevent length checks within the CSV logic.  Handle it out here.
	r.FieldsPerRecord = -1
	var ret []downloadJob
	lines, err := r.ReadAll()
	if err != nil {
		return nil, err
	}
	for n, line := range lines {
		job, err := MakeDlJobFromLine(line)
		if err != nil {
			return nil, fmt.Errorf("Error reading %q, line %d: %w", *flagListFile, n+1, err)
		}
		ret = append(ret, *job)
	}
	return ret, nil
}

func exit(err error) {
	fmt.Println("Fatal error: ", err)
	os.Exit(1)
}

func MakeStatus(err error) Status {
	if err == nil {
		return Finished
	}
	return Error
}

// Download downloads jobs until the jobs channel is closed.  Success and errors
// are signaled through the status channel.
func Download(jobs <-chan downloadJob, status chan<- statusMessage) {
	for {
		job, ok := <-jobs
		if !ok {
			return
		}
		status <- statusMessage{job, Started, nil, 0}
		args := []string{job.URL, "-o", job.Name}
		if *flagReferer != "" {
			args = append(args, "--referer", *flagReferer)
		}
		if *flagUserAgent != "" {
			args = append(args, "--user-agent", *flagUserAgent)
		}
		cmd := exec.Command("youtube-dl.exe", args...)
		start := time.Now()
		err := cmd.Run()
		status <- statusMessage{
			Job:      job,
			Status:   MakeStatus(err),
			Error:    err,
			Duration: time.Now().Sub(start),
		}
	}
}

// HandleStatus indicates if an error occurred, and if progress was made.
func HandleStatus(msg statusMessage) (bool, bool) {
	switch status := msg.Status; status {
	case Started:
		fmt.Printf("Started downloading %q at %s\n", msg.Job.Name, time.Now().Format(DateFormat))
		return true, false
	case Finished:
		fmt.Printf("Finished downloading %q in %s \n", msg.Job.Name, msg.Duration)
		return true, true
	case Error:
		fmt.Printf("Error downloading %q: %s, duration: %s\n", msg.Job.Name, msg.Error, msg.Duration)
		return false, true
	default:
		panic(fmt.Sprintf("Unexpected status: %v", status))
	}
}

func main() {
	flag.Parse()
	if err := ValidateFlags(); err != nil {
		exit(err)
	}
	jobs, err := ReadDownloadJobs()
	if err != nil {
		exit(err)
	}
	queue := make(chan downloadJob, *flagNumJobs)
	status := make(chan statusMessage, *flagNumJobs)
	finished := 0
	failures := []string{}

	go func() {
		for _, job := range jobs {
			queue <- job
		}
		close(queue)
	}()

	for i := 0; i < *flagNumJobs; i += 1 {
		go Download(queue, status)
	}

	for finished != len(jobs) {
		msg := <-status
		ok, progress := HandleStatus(msg)
		if !ok {
			failures = append(failures, msg.Job.Name)
		}
		if progress {
			finished++
			fmt.Println("Progress: ", finished, "/", len(jobs))
		}
	}
	if len(failures) > 0 {
		fmt.Printf("Failed to download %d out of %d files:\n", len(failures), len(jobs))
		for _, failure := range failures {
			fmt.Println(failure)
		}
	}
}
